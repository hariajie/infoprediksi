let table = {};

function init_map() {
  let map = L.map("mapid", { minZoom: 5 }).setView(
    [-1.2828926, 119.1433416],
    5
  );

  //Siapin variable besok
  let tanggal_valid = "";
  L.tileLayer.provider("GoogleHybrid").addTo(map);
  moment.locale("id");

  $.ajax({
    url: "/infoprediksi/api",
    method: "GET",
    dataType: "json",
    beforeSend: function () {
      $("#loading").show();
    },
    success: function (response) {
      let geojson = response.geojson;

      let geoJsonLayer = new L.GeoJSON(geojson, {
        style: function (feature) {
          let cat = feature.properties.category;
          let warna, style;
          cat = parseInt(cat);
          //console.log("Cate :" + cat)

          if (cat == 10) {
            warna = "#db0000";
          } else if (cat > 5) {
            warna = "#f09835";
          } else if (cat > 0) {
            warna = "#fff157";
          }

          style = {
            color: warna,
            weight: 0,
            opacity: 0.7,
            fillOpacity: 0.7,
          };

          return style;
        },
        onEachFeature: function (feature, layer) {
          //Bikin list kab
          let impact = feature.properties.impacted;
          let category = feature.properties.category;

          let tmp = {};
          for (const [key, val] of Object.entries(impact)) {
            tmp[key] = Object.keys(val);
            // for (const [k, v] of Object.entries(impact[key])) {
            //   tmp[key].push(k);
            // }
          }
          let list = {
            prov: tmp,
            status: feature.properties.status,
          };

          let sta;
          if (list.status == "Awas") {
            sta = "<span class='badge awas text-dark'>Awas</span>";
          } else if (list.status == "Siaga") {
            sta = "<span class='badge siaga text-dark'>Siaga</span>";
          } else if (list.status == "Waspada") {
            sta = "<span class='badge waspada text-dark'>Waspada</span>";
          }

          let content =
            "<span style='font-size: 1.2em'><b>Status : " +
            sta +
            "</b></span><br>";
          content +=
            "<span style='font-size: 1.2em'><b>Hujan Lebat Kategori : " +
            category +
            "</b></span>";
          content += "<hr class='mb-3 mt-1'>";

          content +=
            "<nav>\
              <div class='nav nav-tabs' id='nav-tab' role='tablist'>\
                <button class='nav-link active' id='nav-kab-tab' data-bs-toggle='tab' data-bs-target='#nav-kab' type='button' role='tab' aria-controls='nav-kab' aria-selected='true'>Wilayah Terdampak</button>\
                <button class='nav-link' id='nav-matriks-tab' data-bs-toggle='tab' data-bs-target='#nav-matriks' type='button' role='tab' aria-controls='nav-matriks' aria-selected='false'>Matriks Dampak</button>\
              </div>\
            </nav>\
            <div class='tab-content' id='nav-tabContent'>\
          <div class='tab-pane fade show active' id='nav-kab' role='tabpanel' aria-labelledby='nav-kab-tab'>";

          content +=
            "<table class='table table-sm table-responsive table-bordered mt-3' style='font-size: 1.05em'>";
          content += "<thead class='bg-info fw-bold'>";
          content += "<tr><td>Provinsi</td><td>Kab/Kota</td></tr></thead>";
          content += "<tbody>";
          for (const [key, val] of Object.entries(list.prov)) {
            content += "<tr><td>" + key + "</td><td>";
            for (const [k, v] of Object.entries(list.prov[key])) {
              content += v + "<br>";
            }
            content += "</td></tr>";
          }
          content += "</tbody>";
          content += "</table>";
          content += "</div>";

          //Ini Tab Content Matriks Dampak
          content +=
            "<div class='tab-pane fade' id='nav-matriks' role='tabpanel' aria-labelledby='nav-matriks-tab'>";
          content +=
            "<table class='matrix mt-3' cellpadding='0' cellspacing='0' align='center' style='text-align: center;'>\
        <tr>\
            <td class='rotate' rowspan='4' style='border-right: solid 1px #000;'>\
                <div>Likelihood</div>\
            </td>\
            <td class='imp'>High</td>\
            <td class='num aman'></td>\
            <td class='num waspada'>";
          content += (category == 2) ? "<span class='badge bg-secondary'>2</span></td><td class='num siaga'>" : "2" + "</td><td class='num siaga'>";
          content += (category == 7) ? "<span class='badge bg-secondary'>7</span></td><td class='num awas'>" : "7" + "</td><td class='num awas'>";
          content += (category == 10) ? "<span class='badge bg-secondary'>10</span></td>" : "10" + "</td>\
          </tr>\
        <tr>\
            <td class='imp'>Medium</td>\
            <td class='num aman'></td>\
            <td class='num waspada'>";
          content += (category == 1) ? "<span class='badge bg-secondary'>1</span></td><td class='num siaga'>" : "1" + "</td><td class='num siaga'>";
          content += (category == 6) ? "<span class='badge bg-secondary'>6</span></td><td class='num siaga'>" : "6" + "</td><td class='num siaga'>";
          content += (category == 9) ? "<span class='badge bg-secondary'>9</span></td>" : "9" + "</td>\
          </tr>\
        <tr>\
            <td class='imp'>Low</td>\
            <td class='num aman'></td>\
            <td class='num aman'></td>\
            <td class='num waspada'>";
          content += (category == 4) ? "<span class='badge bg-secondary'>4</span></td><td class='num siaga'>" : "4" + "</td><td class='num siaga'>";
          content += (category == 8) ? "<span class='badge bg-secondary'>8</span></td>" : "8" + "</td>";
          content += "</tr>\
        <tr>\
            <td class='imp'>Very Low</td>\
            <td class='num aman'></td>\
            <td class='num aman'></td>\
            <td class='num waspada'>"
          content += (category == 3) ? "<span class='badge bg-secondary'>3</span></td><td class='num waspada'>" : "3" + "</td><td class='num waspada'>";
          content += (category == 5) ? "<span class='badge bg-secondary'>5</span></td>" : "5" + "</td>\
         </tr>\
        <tr>\
            <td colspan='2'></td>\
            <td class='imp'>Minimal</td>\
            <td class='imp'>Mirror</td>\
            <td class='imp'>Significant</td>\
            <td class='imp'>Severe</td>\
        </tr>\
        <tr>\
            <td colspan='2'></td>\
            <td colspan='4' style='border-top: solid 1px #000;'>Impact</td>\
        </tr>\
    </table>";
          content += "</div>";

          //End Tab Content Matriks Dampak
          content += "</div>";
          content += "</div>";

          // console.log(content);

          let popup = L.popup().setContent(content);
          layer.bindPopup(popup, { minWidth: 350 });
        },
      }).addTo(map);
      $("#loading").hide();
      table = response.data;
      tanggal_valid = response.data.validFor;

      $(".tanggal").html(
        "<b>" + moment(tanggal_valid).format("dddd, DD MMMM YYYY") + "</b>"
      );
      list_table(table);
    },
  });

  //Jam akses

  let now = moment().format("dddd, DD MMMM YYYY - HH:mm:ss");

  $(".jam").html("<b>" + now + "</b>");
}

$(document).ready(function () {
  $(".scroll-top").click(function () {
    $("html, body").animate(
      {
        scrollTop: 0,
      },
      "fast"
    );
    return false;
  });
});

init_map();