function list_table(table) {
  let html = "<table class='table table-sm table-responsive table-bordered'>";
  html += "<thead class='bg-navy text-white fw-bold'>";
  html += "<tr><td>Status</td><td>Provinsi</td></tr></thead>";
  html += "<tbody>";
  if (Object.keys(table.awas).length > 0) {
    html += "<tr class='awas'><td>Awas</td>";
    html += "<td>";
    for (const [key, val] of Object.entries(table.awas)) {
      html += val + "<br>";
    }
    html += "</td></tr>";
  }
  if (Object.keys(table.siaga).length > 0) {
    html += "<tr class='siaga'><td>Siaga</td>";
    html += "<td>";
    for (const [key, val] of Object.entries(table.siaga)) {
      html += val + "<br>";
    }
    html += "</td></tr>";
  }
  if (Object.keys(table.waspada).length > 0) {
    html += "<tr class='waspada'><td>Waspada</td>";
    html += "<td>";
    for (const [key, val] of Object.entries(table.waspada)) {
      html += val + "<br>";
    }
    html += "</td></tr>";
  }
  if (
    Object.keys(table.waspada).length == 0 &&
    Object.keys(table.awas).length == 0 &&
    Object.keys(table.siaga).length == 0
  ) {
    html += "<tr class='text-center'><td colspan='2'>Tidak ada data.</td>";
    html += "</td></tr>";
  }
  html += "</tbody>";
  html += "</table>";

  $(".prov-list").html(html);
}
