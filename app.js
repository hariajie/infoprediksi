const express = require("express");
const path = require("path");
const rewrite = require("express-urlrewrite");
const Route = require("./routes/routes");
require("dotenv/config");
const cors = require("cors");


const PORT = process.env.PORT;
const app = express();

//Set assets directory
//const assetsDir = path.join(__dirname, "/assets");
app.use("/infoprediksi", express.static(path.join(__dirname, "assets")));
app.use(cors());

//Set EJS template engine
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));

//Routes
app.get("/", (req, res) => {
  res.redirect("/infoprediksi")
});

app.use("/infoprediksi", Route);


//Run server
app.listen(PORT, () => {
  console.log(`Web Infoprediksi jalan di port ${PORT}`);
})

