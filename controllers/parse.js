const moment = require("moment");

exports.parse = (response, geoonly = false, bravo = false) => {
  let data = {
    data: { validFor: "", awas: [], siaga: [], waspada: [] },
    geojson: {},
  };

  let geojson = { type: "FeatureCollection", features: [] };

  let tmp_awas = {},
    tmp_siaga = {},
    tmp_waspada = {},
    push = {},
    status = "";

  data.data.validFor = response.data[0].validFor.split("T")[0];

  for (let key in response.data) {
    let cat = parseInt(response.data[key].area.properties.category);
    if (cat == 10) {
      status = "Awas";
    } else if (cat > 5) {
      status = "Siaga";
    } else if (cat > 0) {
      status = "Waspada";
    }

    push = {
      validFor: response.data[key].validFor,
      type: "Feature",
      geometry: {
        type: response.data[key].area.geometry.type,
        coordinates: response.data[key].area.geometry.coordinates,
      },
      properties: {
        category: cat,
        impacted: response.data[key].impacted,
        status: status,
      },
    };

    geojson.features.push(push);
    Object.assign(data.geojson, geojson);

    //Insert Data
    if (status == "Awas") {
      let k_awas = Object.keys(response.data[key].impacted);
      Object.assign(tmp_awas, k_awas);
    } else if (status == "Siaga") {
      let k_siaga = Object.keys(response.data[key].impacted);
      Object.assign(tmp_siaga, k_siaga);
    } else if (status == "Waspada") {
      let k_waspada = Object.keys(response.data[key].impacted);
      Object.assign(tmp_waspada, k_waspada);
    }

    //console.log(tmp_awas);
    //console.log(tmp_siaga);
    // console.log(Object.values(tmp_waspada));

    for (let keys in Object.keys(tmp_awas)) {
      data.data.awas.push(tmp_awas[keys]);
    }
    for (let keys in tmp_siaga) {
      data.data.siaga.push(tmp_siaga[keys]);
    }
    for (let keys in Object.keys(tmp_waspada)) {
      data.data.waspada.push(tmp_waspada[keys]);
    }
  }

  //Hapus elemen redundan -> array filter sesuai status di atasnya -> trus chain sort
  let uniq_awas = [...new Set(data.data.awas)];
  data.data.awas = { ...uniq_awas };
  let uniq_siaga = [...new Set(data.data.siaga)];
  data.data.siaga = {
    ...uniq_siaga
      .filter(function (e) {
        return this.indexOf(e) < 0;
      }, uniq_awas)
      .sort(urutin),
  };
  let uniq_waspada = [...new Set(data.data.waspada)];
  data.data.waspada = {
    ...uniq_waspada
      .filter(function (e) {
        return this.indexOf(e) < 0;
      }, uniq_siaga)
      .sort(urutin),
  };


  if (geoonly) {
    return data.geojson;
  }
  else if (bravo) {
    data.bravo = {};
    data.bravo = generate_bravo(data.data);
    return data.bravo;
  } else {
    return data;
  }
};

const urutin = (a, b) => {
  const prov = [
    "Aceh",
    "Sumatera Utara",
    "Sumatera Barat",
    "Riau",
    "Jambi",
    "Sumatera Selatan",
    "Bengkulu",
    "Lampung",
    "Kepulauan Bangka Belitung",
    "Kepulauan Riau",
    "Dki Jakarta",
    "Jawa Barat",
    "Jawa Tengah",
    "Daerah Istimewa Yogyakarta",
    "Jawa Timur",
    "Banten",
    "Bali",
    "Nusa Tenggara Barat",
    "Nusa Tenggara Timur",
    "Kalimantan Barat",
    "Kalimantan Tengah",
    "Kalimantan Selatan",
    "Kalimantan Timur",
    "Kalimantan Utara",
    "Sulawesi Utara",
    "Sulawesi Tengah",
    "Sulawesi Selatan",
    "Sulawesi Tenggara",
    "Gorontalo",
    "Sulawesi Barat",
    "Maluku",
    "Maluku Utara",
    "Papua",
    "Papua Barat",
  ];
  return prov.indexOf(a) - prov.indexOf(b);
};

//Function untuk membuat pesan bravo
const generate_bravo = (data) => {

  let no = 1, result = { validFor: data.validFor, message: "" };
  moment.locale("id");
  let message = "<!DOCTYPE html><meta http-equiv='Content-Type' content='text/html; charset=utf-8'/><p>Potensi Hujan Harian pada&nbsp;hari&nbsp;<strong>" + moment(data.validFor).format("dddd") + "</strong>&nbsp;</strong>tanggal <strong>" + moment(data.validFor).format("DD MMMM YYYY") + "</strong>, wilayah terdampak hujan lebat dapat terjadi di :</p>";
  message += "<p dir='ltr'>";

  if (Object.values(data.awas).length > 0) {
    for (const key in data.awas) {
      message += "<b>" + no++;
      message += ". " + data.awas[key] + " (awas)</b><br />"
    }
  }
  if (Object.values(data.siaga).length > 0) {
    for (const key in data.siaga) {
      message += "<b>" + no++;
      message += ". " + data.siaga[key] + " (siaga)</b><br />"
    }
  }
  if (Object.values(data.waspada).length > 0) {
    for (const key in data.waspada) {
      message += no++;
      message += ". " + data.waspada[key] + " (waspada)<br />"
    }
  }
  message += "</p>";

  message += "<p><a href='https://sitaba.pu.go.id/mitigasi-bencana/?category=Hujan%20Lebat' target='_blank' rel='noopener noreferrer'><span xss='removed'>https://sitaba.pu.go.id/mitigasi-bencana</span></a></p>\
  <p>Call Center Tanggap Bencana-Pusdatin&nbsp;<br />\
  0811-1950-0700<br />\
  tanggapbencana.pusdatin@pu.go.id</p></html>";

  result.message = message;
  return result;

}
