const express = require("express");
const moment = require("moment-timezone");
const axios = require("axios");
const parse = require("../controllers/parse");
const router = express.Router();


//Set Time Zone ke Jakarta/WIB
moment.tz.setDefault("Asia/Jakarta");
router.get("/", async (req, res) => {
    res.render("index");
})

router.get("/api", async function (req, res) {
    //Cek waktu
    const format_jam = "HH:mm:ss";
    let tgl;

    if (moment().isSameOrAfter(moment("13:00:00", format_jam))) {
        tgl = moment().add(1, "days").format("YYYY-MM-DD");
    } else {
        tgl = moment().format("YYYY-MM-DD");
    }
    // console.log("hasil moment :" + tgl);

    tgl += "T00:00:00.000Z";

    let url =
        "https://signature.bmkg.go.id/api/signature/impact/public/list/" + tgl;
    console.log(url);

    await axios.get(url)
        .then((response) => {
            let result = parse.parse(response.data);
            res.send(result);
        })
        .catch((error) => {
            res.status(503).send({ status: "error", message: "Gagal mengambil data." })
        })

});


router.get("/geojson", async function (req, res) {
    //Cek waktu
    const format_jam = "HH:mm:ss";
    let tgl;

    if (moment().isSameOrAfter(moment("13:00:00", format_jam))) {
        tgl = moment().add(1, "days").format("YYYY-MM-DD");
    } else {
        tgl = moment().format("YYYY-MM-DD");
    }
    // console.log("hasil moment :" + tgl);

    tgl += "T00:00:00.000Z";
    let url =
        "https://signature.bmkg.go.id/api/signature/impact/public/list/" + tgl;
    // console.log(url);

    await axios.get(url)
        .then((response) => {
            let result = parse.parse(response.data, true);
            res.send(result);
        })
        .catch((error) => {
            res.status(503).send({ status: "error", message: "Gagal mengambil data." })
        })


});

router.get("/api/bravo", async function (req, res) {
    //Cek waktu
    const format_jam = "HH:mm:ss";
    let tgl;

    if (moment().isSameOrAfter(moment("13:00:00", format_jam))) {
        tgl = moment().add(1, "days").format("YYYY-MM-DD");
    } else {
        tgl = moment().format("YYYY-MM-DD");
    }
    // console.log("hasil moment :" + tgl);

    tgl += "T00:00:00.000Z";

    let url =
        "https://signature.bmkg.go.id/api/signature/impact/public/list/" + tgl;
    console.log(url);

    await axios.get(url)
        .then((response) => {
            let result = parse.parse(response.data, false, true);
            res.send(result);
        })
        .catch((error) => {
            res.status(503).send({ status: "error", message: "Gagal mengambil data." })
        })


});

router.get("/api/:tanggal", async function (req, res) {
    let url = "https://signature.bmkg.go.id/api/signature/impact/public/list/";
    url += req.params.tanggal;
    url += "T00:00:00.000Z";
    // console.log(url);

    await axios.get(url)
        .then((response) => {
            let result = parse.parse(response.data);
            res.send(result);
        })
        .catch((error) => {
            res.status(503).send({ status: "error", message: "Gagal mengambil data." })
        })

});

router.get("/:tanggal/geojson", async function (req, res) {
    let url =
        "https://signature.bmkg.go.id/api/signature/impact/public/list/" +
        req.params.tanggal +
        "T00:00:00.000Z";
    // console.log(url);

    await axios.get(url)
        .then((response) => {
            let result = parse.parse(response.data, true);
            res.send(result);
        })
        .catch((error) => {
            res.status(503).send({ status: "error", message: "Gagal mengambil data." })
        })

});


module.exports = router;